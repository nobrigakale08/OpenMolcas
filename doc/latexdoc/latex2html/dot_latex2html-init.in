# $ this file belongs to the Molcas repository $
#LaTeX2HTML Version 96.1 : dot.latex2html-init
#
### Command Line Argument Defaults #######################################

$FONT_SIZE = '12pt';
$INDEX_TABLE_WIDTH  = 450;
$CONTENTS_TABLE_WIDTH  = 450;
$INDEX_WIDTH  = 250;
$CONTENTS_WIDTH  = 250;
$HTML_VALIDATE = '';
$LOCAL_ICONS = 1;
$TOC_STARS = 1;
$SHORT_INDEX=1;
#$CUSTOM_BUTTONS='<a href="/molcas/"><img src="/images/molcashome.gif" alt="Molcas Home"></a>';
$NEXT_GROUP=1;
$PREVIOUS_GROUP=1;

# Replace simple math with HTML tags,
# split complex math into tags and images.
$HTML_VERSION='3.2,latin1,unicode';
$NO_SIMPLE_MATH=0;

# Work around gray PNG generation quirks
# $ANTI_ALIAS_TEXT=0;


#$MAX_LINK_DEPTH = 2;    # Stop showing child nodes at this depth 

# TOC and Main file: appearence
$MAX_SPLIT_DEPTH = @@MAX_SPLIT@@;	# Stop making separate files at this depth
$MAX_LINK_DEPTH = @@MAX_LINK@@;    # Stop showing child nodes at this depth   
$TOC_DEPTH=@@TOC_DEPTH@@;

$NOLATEX = 0;           # 1 = do not pass unknown environments to Latex

$EXTERNAL_IMAGES = 0;   # 1 = leave the images outside the document 

$ASCII_MODE = 0;        # 1 = do not use any icons or internal images

# 1 =  use links to external postscript images rather than inlined bitmap
# images.
$PS_IMAGES = 0;

$TITLE = $default_title;      # The default is "No Title" 

$DESTDIR = '@@DEST_DIR@@';         # Put the result in this directory 

# When this is set, the generated HTML files will be placed in the 
# current directory. If set to 0 the default behaviour is to create (or reuse)
# another file directory.
$NO_SUBDIR = 0;


$NO_NAVIGATION = 0;	# 1 = do not put a navigation panel at the top of each page

# Put navigation links at the top of each  page.  If  the page  exceeds
# $WORDS_IN_PAGE  number of words then put one at the bottom of the page.
@@BEGIN_SIMPLE_NAVIGATION_PANELS@@
$AUTO_NAVIGATION = 1;

# Supply your own string if you don't like the default <Name> <Date>
$ADDRESS = '<I>(C) Lund University, 2014</I><p><a href="http://www.molcas.org/">Back to Molcas home</a>';
@@END_SIMPLE_NAVIGATION_PANELS@@

@@BEGIN_SITE_NAVIGATION_PANELS@@
$AUTO_NAVIGATION = 0;
$TOP_NAVIGATION = 1;
$BOTTOM_NAVIGATION = 1;

$ICONSERVER='/images';

# Supply your own string if you don't like the default <Name> <Date>
$ADDRESS = '<!-- (C) Lund University, 2014 -->';
@@END_SITE_NAVIGATION_PANELS@@

# Put a link to the index page in  the  navigation  panel
$INDEX_IN_NAVIGATION = 1;

# Put a link to the table of contents  in  the  navigation  panel
$CONTENTS_IN_NAVIGATION = 1;

# Put a link to the next logical page  in  the  navigation  panel
$NEXT_PAGE_IN_NAVIGATION = 1;

# Put a link to the previous logical page  in  the  navigation  panel
$PREVIOUS_PAGE_IN_NAVIGATION = 1;

$INFO = 1;              # 0 = do not make a "About this document..." section 

# Reuse images generated during previous runs
$REUSE = 2;

# When this is 1, the section numbers are shown. The section numbers should 
# then match those that would have bee produced by LaTeX.
# The correct section numbers are obtained from the $FILE.aux file generated 
# by LaTeX.
# Hiding the seciton numbers encourages use of particular sections 
# as standalone documents. In this case the cross reference to a section 
# is shown using the default symbol rather than the section number.
$SHOW_SECTION_NUMBERS = 1;

### Other global variables ###############################################
$CHILDLINE = "<BR> <HR>\n";

# This is the line width measured in pixels and it is used to right justify
# equations and equation arrays; 
$LINE_WIDTH = 500;		

# Used in conjunction with AUTO_NAVIGATION
$WORDS_IN_PAGE = 300;	

# Affects ONLY the way accents are processed 
$default_language = 'english';	

# The value of this variable determines how many words to use in each 
# title that is added to the navigation panel (see below)
# 
$WORDS_IN_NAVIGATION_PANEL_TITLES = 4;

# This number will determine the size of the equations, special characters,
# and anything which will be converted into an inlined image
# *except* "image generating environments" such as "figure", "table" 
# or "minipage".
# Effective values are those greater than 0.
# Sensible values are between 0.1 - 4.
$MATH_SCALE_FACTOR = 1.5;

# This number will determine the size of 
# image generating environments such as "figure", "table" or "minipage".
# Effective values are those greater than 0.
# Sensible values are between 0.1 - 4.
$FIGURE_SCALE_FACTOR = 1.4;

#  If this is set then intermediate files are left for later inspection.
#  This includes $$_images.tex and $$_images.log created during image
#  conversion.
#  Caution: Intermediate files can be *enormous*.
#$DEBUG = 1;
#$VERBOSITY=10;


#  If both of the following two variables are set then the "Up" button
#  of the navigation panel in the first node/page of a converted document
#  will point to $EXTERNAL_UP_LINK. $EXTERNAL_UP_TITLE should be set
#  to some text which describes this external link.
$EXTERNAL_UP_LINK = "http://www.molcas.org/";
$EXTERNAL_UP_TITLE = "Molcas Home";

# If this is set then the resulting HTML will look marginally better if viewed 
# with Netscape.
$NETSCAPE_HTML = 1;

# Valid paper sizes are "letter", "legal", "a4","a3","a2" and "a0"
# Paper sizes has no effect other than in the time it takes to create inlined
# images and in whether large images can be created at all ie
#  - larger paper sizes *MAY* help with large image problems 
#  - smaller paper sizes are quicker to handle
$PAPERSIZE = "a4";

# Replace "english" with another language in order to tell LaTeX2HTML that you 
# want some generated section titles (eg "Table of Contents" or "References")
# to appear in a different language. Currently only "english" and "french"
# is supported but it is very easy to add your own. See the example in the
# file "latex2html.config" 
$TITLES_LANGUAGE = "english";

### Navigation Panel ##########################################################
#
# The navigation panel is constructed out of buttons and section titles.
# These can be configured in any combination with arbitrary text and 
# HTML tags interspersed between them. 
# The buttons available are:
# $PREVIOUS - points to the previous section
# $UP  - points up to the "parent" section
# $NEXT - points to the next section
# $NEXT_GROUP - points to the next "group" section
# $PREVIOUS_GROUP - points to the previous "group" section
# $CONTENTS - points to the contents page if there is one
# $INDEX - points to the index page if there is one
#
# If the corresponding section exists the button will contain an
# active link to that section. If the corresponding section does
# not exist the button will be inactive.
#
# Also for each of the $PREVIOUS $UP $NEXT $NEXT_GROUP and $PREVIOUS_GROUP
# buttons there are equivalent $PREVIOUS_TITLE, $UP_TITLE, etc variables
# which contain the titles of their corresponding sections. 
# Each title is empty if there is no corresponding section.
#
# The subroutine below constructs the navigation panels in each page.
# Feel free to mix and match buttons, titles, your own text, your logos,
# and arbitrary HTML (the "." is the Perl concatenation operator).

@@BEGIN_SIMPLE_NAVIGATION_PANELS@@

$BODYTEXT='TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#0A1252" VLINK="#957B52"';

sub top_navigation_panel {
"<Font size=+2>@@DOC_NAME@@:</font>" .

    # Now add a few buttons with a space between them
    "$NEXT $UP $PREVIOUS $CONTENTS $INDEX $CUSTOM_BUTTONS" .
    
    "<BR>\n" .		# Line break
	
    # If ``next'' section exists, add its title to the navigation panel
    ($NEXT_TITLE ? "<B> Next:</B> $NEXT_TITLE\n" : undef) . 
    
    # Similarly with the ``up'' title ...
    ($UP_TITLE ? "<B>Up:</B> $UP_TITLE\n" : undef) . 
 
    # ... and the ``previous'' title
    ($PREVIOUS_TITLE ? "<B> Previous:</B> $PREVIOUS_TITLE\n" : undef) .
   
    #  Line Break, horizontal rule (3-d dividing line) and new paragraph  
    "<BR> <P>\n"		
}
@@END_SIMPLE_NAVIGATION_PANELS@@

@@BEGIN_SITE_NAVIGATION_PANELS@@

$BODYTEXT='body TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#0A1252" VLINK="#957B52" MarginWidth="0" MarginHeight="0"';

sub top_navigation_panel {
 my ($My_Next,$My_Up,$My_Prev,$My_Contents,$My_Index)=('','','','','');

 # Pictures for top of page:
 # 'next'
 if ($NEXT=~/href="(.*?)"/i) {
   $My_Next=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/next_top.gif" alt="next" border=0 width=120 height=120>).
            qq(</a>);
 }

 # 'up'
 if ($UP=~/href="(.*?)"/i) {
   $My_Up=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/up_top.gif" alt="up" border=0 width=120 height=120>).
            qq(</a>);
 }

 # 'previous'
 if ($PREVIOUS=~/href="(.*?)"/i) {
   $My_Prev=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/prev_top.gif" alt="previous" border=0 width=120 height=120>).
            qq(</a>);
 }

 # 'contents'
 if ($CONTENTS=~/href="(.*?)"/i) {
   $My_Contents=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/contents_top.gif" alt="contents" border=0 width=120 height=120>).
            qq(</a>);
 }

 # 'index'
 if ($INDEX=~/href="(.*?)"/i) {
   $My_Index=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/index_top.gif" alt="index" border=0 width=120 height=120>).
            qq(</a>);
 }
 
 return <<"-===========END_OF_HEADER===========-"
<!--- BEGIN OF TOP_NAVIGATION PANEL -->
<style TYPE="text/css"> 
  A.red { color: red; 
            font-family: Arial, Helvetica, sans-serif; 
            font-size: 12pt; 
            text-decoration: none; 
          } 
  A.darkred { color: darkred; 
            font-family: Arial, Helvetica, sans-serif; 
            font-size: 12pt; 
            text-decoration: none; 
          } 
  A.blue { color: blue; 
            font-family: Arial, Helvetica, sans-serif; 
            font-size: 12pt; 
            text-decoration: none; 
          } 
   A.green { color: green; 
            font-family: Arial, Helvetica, sans-serif; 
            font-size: 11pt; 
            text-decoration: none; 
          } 

</STYLE> 

<!-- body TEXT="#000000" BGCOLOR="#FFFFFF" LINK="#0A1252" VLINK="#957B52" MarginWidth="0" MarginHeight="0" -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="100%">

<tr><td nowrap colspan="2" valign=center 
background="/molcas/img/hbar1.gif"><a href="/"><img 
width="218" height="152" border=0 src="/molcas/img/documentation_logo.gif"
></a><img src="/images/dot.gif" height="20" width="20"
>$My_Next<img src="/images/dot.gif" height="20" width="20"
>$My_Up<img src="/images/dot.gif" height="20" width="20"
>$My_Prev<img src="/images/dot.gif" height="20" width="20"
>$My_Contents<img src="/images/dot.gif" height="20" width="20"
>$My_Index<img src="/images/dot.gif" height="20" width="20"
></td>
<td><img src="/molcas/img/t_r.gif"></td></tr>

<tr><td  valign="top" width="174" background="/molcas/img/vbar.gif" height="100%">
<img src="/images/dot.gif" height="10" width="174">

<img border=0 src='/molcas/img/Features.gif'><a href=/introduction.html><img border=0 src='/molcas/img/About.gif'></a><a href=/methods.html><img border=0 src='/molcas/img/Methods.gif'></a><a href=/applications.html><img border=0 src='/molcas/img/Applications.gif'></a><a href=/showcase.html><img border=0 src='/molcas/img/Showcase.gif'></a><img border=0 src='/molcas/img/Code.gif'><a href=/order.html><img border=0 src='/molcas/img/Howtoorder.gif'></a><a href=/download.html><img border=0 src='/molcas/img/Downloads.gif'></a><a href=/GUI.html><img border=0 src='/molcas/img/GUI.gif'></a><a href=/hardware.html><img border=0 src='/molcas/img/Hardware.gif'></a><img border=0 src='/molcas/img/Help.gif'><a href='/documentation/manual/'><img border=0 src='/molcas/img/Manual.gif'></a><a href=/tutorials.html><img border=0 src='/molcas/img/Tutorials.gif'></a><a href=/helpline.html><img border=0 src='/molcas/img/Helpline.gif'></a><a href=/faq.html><img border=0 src='/molcas/img/FAQ.gif'></a><img border=0 src='/molcas/img/People.gif'><img border=0 src='/molcas/img/Contact.gif'><a href=/contributors.html><img border=0 src='/molcas/img/Contributors.gif'></a><a href=/roos.html><img border=0 src='/molcas/img/B.O.Roos.gif'></a></td>

<td width="100%" valign="top">

<table border="0" cellpadding="0" cellspacing="0" width="100%" height="30%">
<tr>
<td>
<Font size=+2>@@DOC_NAME@@:</font><br>
<img src="/images/dot.gif" hspace=350><br>
-===========END_OF_HEADER===========-
 
  # . "$NEXT $UP $PREVIOUS $CONTENTS $INDEX $CUSTOM_BUTTONS<br>"
  # If ``next'' section exists, add its title to the navigation panel
 . ($NEXT_TITLE ? "<B> Next:</B> $NEXT_TITLE\n" : undef)
  
  # Similarly with the ``up'' title ...
  . ($UP_TITLE ? "<B>Up:</B> $UP_TITLE\n" : undef)
 
  # ... and the ``previous'' title
  . ($PREVIOUS_TITLE ? "<B> Previous:</B> $PREVIOUS_TITLE\n" : undef)
   
  #  Line Break, horizontal rule (3-d dividing line) and new paragraph  
  . "<BR> <P>\n"
  . "<!--- END OF TOP_NAVIGATION PANEL -->\n"
  ;
}
@@END_SITE_NAVIGATION_PANELS@@


@@BEGIN_SIMPLE_NAVIGATION_PANELS@@
sub bot_navigation_panel {
    #  Start with a horizontal rule (3-d dividing line)
    return "<HR>"
    
    # Now add a few buttons with a space between them
    . "$NEXT $UP $PREVIOUS $CONTENTS $INDEX"
    
    . "<BR>\n"	# Line break
	
    # If ``next'' section exists, add its title to the navigation panel
    . ($NEXT_TITLE ? "<B> Next:</B> $NEXT_TITLE\n" : '')
    
    # Similarly with the ``up'' title ...
    . ($UP_TITLE ? "<B>Up:</B> $UP_TITLE\n" : '')
 
    # ... and the ``previous'' title
    . ($PREVIOUS_TITLE ? "<B> Previous:</B> $PREVIOUS_TITLE\n" : '')
    ;
}
@@END_SIMPLE_NAVIGATION_PANELS@@

@@BEGIN_SITE_NAVIGATION_PANELS@@
sub bot_navigation_panel {

 my ($My_Next,$My_Up,$My_Prev,$My_Contents,$My_Index)=('','','','','');

 # Pictures for bottom of page:
 # 'next'
 if ($NEXT=~/href="(.*?)"/i) {
   $My_Next=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/next_bottom.gif" alt="next" border=0 width=120 height=30>).
            qq(</a>);
 }

 # 'up'
 if ($UP=~/href="(.*?)"/i) {
   $My_Up=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/up_bottom.gif" alt="up" border=0 width=120 height=30>).
            qq(</a>);
 }

 # 'previous'
 if ($PREVIOUS=~/href="(.*?)"/i) {
   $My_Prev=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/prev_bottom.gif" alt="previous" border=0 width=120 height=30>).
            qq(</a>);
 }

 # 'contents'
 if ($CONTENTS=~/href="(.*?)"/i) {
   $My_Contents=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/contents_bottom.gif" alt="contents" border=0 width=120 height=30>).
            qq(</a>);
 }

 # 'index'
 if ($INDEX=~/href="(.*?)"/i) {
   $My_Index=qq(<a href="$1">).
            qq(<img src="$ICONSERVER/index_bottom.gif" alt="index" border=0 width=120 height=30>).
            qq(</a>);
 }
 

    return
    "\n<!--- BEGIN OF BOTTOM_NAVIGATION PANEL -->"
    
    #  Start with a horizontal rule (3-d dividing line)
    . "<HR>"
    
    # Now add a few buttons with a space between them
    . "$My_Next $My_Up $My_Prev $My_Contents $My_Index"
    
    . "<BR>\n"		# Line break
	
    # If ``next'' section exists, add its title to the navigation panel
    . ($NEXT_TITLE ? "<B> Next:</B> $NEXT_TITLE\n" : undef)
    
    # Similarly with the ``up'' title ...
    . ($UP_TITLE ? "<B>Up:</B> $UP_TITLE\n" : undef)
 
    # ... and the ``previous'' title
    . ($PREVIOUS_TITLE ? "<B> Previous:</B> $PREVIOUS_TITLE\n" : undef)
    
    . <<"-==========END_OF_FOOTER==========-";
</tr>
</td>
</table>
<td background="/molcas/img/r_line.gif">
&nbsp;
</td>
</tr>
<tr>
<td colspan="2" background="/molcas/img/b_line.gif"><img border=0 
src="/molcas/img/b_l.gif"></td><td background="/molcas/img/b_line.gif"><img
src="/molcas/img/b_r.gif"></td></tr>
</table>


</td></tr>
</table>
<!--- END OF BOTTOM_NAVIGATION PANEL -->
-==========END_OF_FOOTER==========-
}
@@END_SITE_NAVIGATION_PANELS@@



# molcas.perl-specific configuration (to be read by molcas.perl)
$MOLCAS_STYLE={
 VISIBLE_SPACE_IMG => '@@L2H_DIR@@/visible_space.png',
};

# Undocumented feature -- set background color (to white).
# To work around "gray PNG" problem.
$LATEX_COLOR='\\pagecolor[gray]{1.0}\\nopagebreak';

1;	# This must be the last line

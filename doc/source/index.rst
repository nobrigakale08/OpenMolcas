Contents
========

.. note::

   Most of this documentation precedes the creation of OpenMolcas.
   It is probably outdated in several points, and it may mention
   features not available in OpenMolcas.

.. toctree::
   :maxdepth: 3
   :numbered:

   intro
   installation.guide/ig
   tutorials/tut
   users.guide/ug
   advanced.examples/ae

.. only:: html

  * :ref:`references`
  * :ref:`genindex`

.. * :ref:`search`

.. toctree::
   :hidden:

   references
